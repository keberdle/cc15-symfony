<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase
{

    /*
     * Otestujte, že uživatel bez role ROLE_ADMIN nemá přístup do /admin sekce
     */
    public function testDisableAnonymousForAdminSites()
    {

        $client = static::createClient();

        $client->request('GET', '/admin');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());

    }

    public function testRoleAdminAccessibleAdminSites()
    {

        $client = static::createClient(array(), array(

            'PHP_AUTH_USER' => 'admin',

            'PHP_AUTH_PW' => 'heslo',

        ));

        $client->request('GET', '/admin');

        $this->assertTrue($client->getResponse()->isRedirect());

    }
}
