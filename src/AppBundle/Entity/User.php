<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 *
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $useId;

    /**
     * @var string
     *
     * @ORM\Column(name="useName", type="string", length=128, nullable=false)
     */
    private $useName;

    /**
     * @var string
     *
     * @ORM\Column(name="useUsername", type="string", length=128, nullable=false)
     */
    private $useUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="useEmail", type="string", length=255, nullable=false)
     */
    private $useEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="usePassword", type="string", length=255, nullable=false)
     */
    private $usePassword;

    /**
     * @ORM\OneToMany(targetEntity="CourseOrder", mappedBy="user")
     */
    private $orders;

    /**
     * Get useId
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->useId;
    }

    /**
     * Set useName
     *
     * @param string $useName
     * @return User
     */
    public function setUseName($useName)
    {
        $this->useName = $useName;

        return $this;
    }

    /**
     * Get useName
     *
     * @return string
     */
    public function getUseName()
    {
        return $this->useName;
    }

    /**
     * Set useUsername
     *
     * @param string $useUsername
     * @return User
     */
    public function setUseUsername($useUsername)
    {
        $this->useUsername = $useUsername;

        return $this;
    }

    /**
     * Get useUsername
     *
     * @return string
     */
    public function getUseUsername()
    {
        return $this->useUsername;
    }

    /**
     * Set useEmail
     *
     * @param string $useEmail
     * @return User
     */
    public function setUseEmail($useEmail)
    {
        $this->useEmail = $useEmail;

        return $this;
    }

    /**
     * Get useEmail
     *
     * @return string
     */
    public function getUseEmail()
    {
        return $this->useEmail;
    }

    /**
     * Set usePassword
     *
     * @param string $usePassword
     * @return User
     */
    public function setUsePassword($usePassword)
    {
        $this->usePassword = $usePassword;

        return $this;
    }

    /**
     * Get usePassword
     *
     * @return string
     */
    public function getUsePassword()
    {
        return $this->usePassword;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add orders
     *
     * @param \AppBundle\Entity\CourseOrder $orders
     * @return User
     */
    public function addOrder(\AppBundle\Entity\CourseOrder $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \AppBundle\Entity\CourseOrder $orders
     */
    public function removeOrder(\AppBundle\Entity\CourseOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
