<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseOrder
 *
 * @ORM\Table(name="course_order")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CourseOrderRepository")
 *
 */
class CourseOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ordId;

    /**
     * @var string
     *
     * @ORM\Column(name="ordState", type="string", length=64, nullable=false)
     */
    private $ordState;

    /**
     * @var string
     *
     * @ORM\Column(name="ordCompany", type="string", length=128, nullable=false)
     */
    private $ordCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="ordComment", type="string", length=255, nullable=false)
     */
    private $ordComment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ordCreate", type="datetime", nullable=false)
     *
     */
    private $ordCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ordDateUpdate", type="datetime", nullable=false)
     *
     */
    private $ordDateUpdate;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders", cascade={"persist"})
     */
    private $user;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="orders", cascade={"persist"})
     */
    private $course;

    /**
     * Get ordId
     *
     * @return integer
     */
    public function getOrdId()
    {
        return $this->ordId;
    }

    /**
     * Set ordState
     *
     * @param string $ordState
     * @return CourseOrder
     */
    public function setOrdState($ordState)
    {
        $this->ordState = $ordState;

        return $this;
    }

    /**
     * Get ordState
     *
     * @return string
     */
    public function getOrdState()
    {
        return $this->ordState;
    }

    /**
     * Set ordCompany
     *
     * @param string $ordCompany
     * @return CourseOrder
     */
    public function setOrdCompany($ordCompany)
    {
        $this->ordCompany = $ordCompany;

        return $this;
    }

    /**
     * Get ordCompany
     *
     * @return string
     */
    public function getOrdCompany()
    {
        return $this->ordCompany;
    }

    /**
     * Set ordComment
     *
     * @param string $ordComment
     * @return CourseOrder
     */
    public function setOrdComment($ordComment)
    {
        $this->ordComment = $ordComment;

        return $this;
    }

    /**
     * Get ordComment
     *
     * @return string
     */
    public function getOrdComment()
    {
        return $this->ordComment;
    }

    /**
     * Set ordCreate
     *
     * @param \DateTime $ordCreate
     * @return CourseOrder
     */
    public function setOrdCreate($ordCreate)
    {
        $this->ordCreate = $ordCreate;

        return $this;
    }

    /**
     * Get ordCreate
     *
     * @return \DateTime
     */
    public function getOrdCreate()
    {
        return $this->ordCreate;
    }

    /**
     * Set ordDateUpdate
     *
     * @param \DateTime $ordDateUpdate
     * @return CourseOrder
     */
    public function setOrdDateUpdate($ordDateUpdate)
    {
        $this->ordDateUpdate = $ordDateUpdate;

        return $this;
    }

    /**
     * Get ordDateUpdate
     *
     * @return \DateTime
     */
    public function getOrdDateUpdate()
    {
        return $this->ordDateUpdate;
    }

    ## PERSIST

    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->ordDateUpdate = new \DateTime(date('Y-m-d H:i:s'));
    }


    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->ordDateUpdate = new \DateTime(date('Y-m-d H:i:s'));
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return CourseOrder
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set course
     *
     * @param \AppBundle\Entity\Course $course
     * @return CourseOrder
     */
    public function setCourse(\AppBundle\Entity\Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \AppBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }
}
