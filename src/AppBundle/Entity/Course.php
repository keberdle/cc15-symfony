<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CourseRepository")
 *
 */
class Course
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     */

    private $couId;

    /**
     * @var string
     *
     * @ORM\Column(name="couCode", type="string", length=32, nullable=false)
     */
    private $couCode;

    /**
     * @var string
     *
     * @ORM\Column(name="couLang", type="string", length=32, nullable=false)
     */
    private $couLang;

    /**
     * @var string
     *
     * @ORM\Column(name="couName", type="string", length=128, nullable=false)
     */
    private $couName;

    /**
     * @var string
     *
     * @ORM\Column(name="couDescription", type="text", nullable=false)
     */
    private $couDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="couFrom", type="datetime", nullable=false)
     */
    private $couFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="couTo", type="datetime", nullable=false)
     */
    private $couTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="couActive", type="integer", nullable=true)
     */
    private $couActive;

    /**
     * @var float
     *
     * @ORM\Column(name="couSale", type="decimal", nullable=true)
     */
    private $couSale;

    /**
     * @var float
     *
     * @ORM\Column(name="couPrice", type="decimal")
     */
    private $couPrice;

    /**
     * @ORM\OneToMany(targetEntity="CourseOrder", mappedBy="course")
     */
    private $orders;


    /**
     * @var \CourseCategory
     * @ORM\ManyToMany(targetEntity="CourseCategory", inversedBy="courses", cascade={"persist"})
     */

    private $categories;


    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get couId
     *
     * @return integer
     */
    public function getCouId()
    {
        return $this->couId;
    }

    /**
     * Set couCode
     *
     * @param string $couCode
     * @return Course
     */
    public function setCouCode($couCode)
    {
        $this->couCode = $couCode;

        return $this;
    }

    /**
     * Get couCode
     *
     * @return string
     */
    public function getCouCode()
    {
        return $this->couCode;
    }

    /**
     * Set couLang
     *
     * @param string $couLang
     * @return Course
     */
    public function setCouLang($couLang)
    {
        $this->couLang = $couLang;

        return $this;
    }

    /**
     * Get couLang
     *
     * @return string
     */
    public function getCouLang()
    {
        return $this->couLang;
    }

    /**
     * Set couName
     *
     * @param string $couName
     * @return Course
     */
    public function setCouName($couName)
    {
        $this->couName = $couName;

        return $this;
    }

    /**
     * Get couName
     *
     * @return string
     */
    public function getCouName()
    {
        return $this->couName;
    }

    /**
     * Set couDescription
     *
     * @param string $couDescription
     * @return Course
     */
    public function setCouDescription($couDescription)
    {
        $this->couDescription = $couDescription;

        return $this;
    }

    /**
     * Get couDescription
     *
     * @return string
     */
    public function getCouDescription()
    {
        return $this->couDescription;
    }

    /**
     * Set couFrom
     *
     * @param \DateTime $couFrom
     * @return Course
     */
    public function setCouFrom($couFrom)
    {
        $this->couFrom = $couFrom;

        return $this;
    }

    /**
     * Get couFrom
     *
     * @return \DateTime
     */
    public function getCouFrom()
    {
        return $this->couFrom;
    }

    /**
     * Set couTo
     *
     * @param \DateTime $couTo
     * @return Course
     */
    public function setCouTo($couTo)
    {
        $this->couTo = $couTo;

        return $this;
    }

    /**
     * Get couTo
     *
     * @return \DateTime
     */
    public function getCouTo()
    {
        return $this->couTo;
    }

    /**
     * Set couActive
     *
     * @param integer $couActive
     * @return Course
     */
    public function setCouActive($couActive)
    {
        $this->couActive = $couActive;

        return $this;
    }

    /**
     * Get couActive
     *
     * @return integer
     */
    public function getCouActive()
    {
        return $this->couActive;
    }

    /**
     * Add orders
     *
     * @param \AppBundle\Entity\CourseOrder $orders
     * @return Course
     */
    public function addOrder(\AppBundle\Entity\CourseOrder $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \AppBundle\Entity\CourseOrder $orders
     */
    public function removeOrder(\AppBundle\Entity\CourseOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add categories
     *
     * @param \AppBundle\Entity\CourseCategory $categories
     * @return Course
     */
    public function addCategory(\AppBundle\Entity\CourseCategory $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \AppBundle\Entity\CourseCategory $categories
     */
    public function removeCategory(\AppBundle\Entity\CourseCategory $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add categoies
     *
     * @param \AppBundle\Entity\CourseCategory $categoies
     * @return Course
     */
    public function addCategoie(\AppBundle\Entity\CourseCategory $categoies)
    {
        $this->categoies[] = $categoies;

        return $this;
    }

    /**
     * Remove categoies
     *
     * @param \AppBundle\Entity\CourseCategory $categoies
     */
    public function removeCategoie(\AppBundle\Entity\CourseCategory $categoies)
    {
        $this->categoies->removeElement($categoies);
    }

    /**
     * Get categoies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategoies()
    {
        return $this->categoies;
    }

    /**
     * Set couSale
     *
     * @param string $couSale
     * @return Course
     */
    public function setCouSale($couSale)
    {
        $this->couSale = $couSale;

        return $this;
    }

    /**
     * Get couSale
     *
     *
     * @return string
     */
    public function getCouSale()
    {
        return $this->couSale;
    }

    /**
     * Set couPrice
     *
     * @param string $couPrice
     * @return Course
     */
    public function setCouPrice($couPrice)
    {
        $this->couPrice = $couPrice;

        return $this;
    }

    /**
     * Get couPrice
     *
     * @return string
     */
    public function getCouPrice()
    {
        return $this->couPrice;
    }
}
