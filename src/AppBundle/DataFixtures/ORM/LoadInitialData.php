<?php
/**
 * Created by PhpStorm.
 * User: kenik
 * Date: 3.8.15
 * Time: 14:10
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CourseCategory;
use AppBundle\Entity\CourseOrder;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Course;
use Symfony\Component\Yaml\Yaml;

class LoadInitialData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {


        $courses = Yaml::parse(file_get_contents(__DIR__ . '/data/courses.yml'));

        foreach ($courses['courses'] as $course) {

            $c = new Course();
            $c->setCouActive($course['active']);
            $c->setCouCode($course['code']);
            $c->setCouDescription($course['description']);
            $c->setCouFrom(new \DateTime($course['from']));
            $c->setCouTo(new \DateTime($course['to']));
            $c->setCouLang($course['lang']);
            $c->setCouName($course['title']);
            $c->setCouPrice($course['price']);
            $c->setCouSale($course['sale']);

            foreach ($course['categories'] as $cat) {
                $c_c = new CourseCategory();
                $c_c->setCatName($cat);
                $c_c->addCourse($c);
                $manager->persist($c_c);
            }

            $manager->persist($c);

        }

        $manager->flush();

        $u = new User();
        $u->setUseEmail('keberdle@gmail.com');
        $u->setUseName('Vasek');
        $u->setUsePassword('');
        $u->setUseUsername('Keberdle');

        $repository = $manager->getRepository('AppBundle:Course');

        $c = $repository->findByCouCode('AJ015');

        $o = new CourseOrder();
        $o->setUser($u);
        $o->setCourse($c[0]);
        $o->setOrdCompany('2Hmoto s.r.o.');
        $o->setOrdState('create');
        $o->setOrdCreate(new \DateTime("now"));
        $o->setOrdDateUpdate(new \DateTime("now"));
        $o->setOrdComment('Poznamka');

        $manager->persist($o);
        $manager->flush();


    }


}