<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */

class AdminController extends Controller
{

    /**
     * @Route("/", name="admin")
     */
    public function indexAction()
    {
        return $this->redirectToRoute("invoices");
    }

    /**
     *
     * @Route("/faktury", name="invoices")
     */
    public function ordersAction()
    {
        return $this->render('admin/invoices.html.twig');
    }

    /**
     *
     * @Route("/objednavky", name="orders")
     */
    public function invoicesAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repository = $em->getRepository('AppBundle:CourseOrder');
        $orders = $repository->getAllCourses();

        return $this->render('admin/orders.html.twig', ['orders' => $orders]);
    }

}