<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     *
     * @Route("/kurzy", name="course")
     */
    public function courseAction()
    {

        $em = $this->get('doctrine')->getManager();
        $repository = $em->getRepository('AppBundle:Course');
        $courses = $repository->getCoursesSortedByCode();

        return $this->render('default/course.html.twig', ['courses' => $courses]);
    }

    /**
     *
     * @Route("/kurz-objednavka/{courseCode}", name="course_form")
     */
    public function courseFormAction($courseCode)
    {
        return $this->render('default/course_form.html.twig', ['courseCode' => $courseCode]);
    }

    /**
     *
     * @Route("/podminky", name="conditions")
     */
    public function conditionsAction()
    {
        return $this->render('default/conditions.html.twig');
    }

    /**
     * @var categorie
     * pro vypisování aktuálních akci zlevnených kurzu
     *
     */
    public function salesAction($category = null)
    {

        $em = $this->get('doctrine')->getManager();
        $repository = $em->getRepository('AppBundle:Course');
        $courses = $repository->getSales($category);

        return $this->render('snipeds/sales.html.twig', ['courses' => $courses]);
    }
}
