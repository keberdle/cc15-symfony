<?php
/**
 * Created by PhpStorm.
 * User: kenik
 * Date: 3.8.15
 * Time: 12:13
 */
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CourseRepository extends EntityRepository
{

    public function getCoursesSortedByCode()
    {

        $qb = $this->createQueryBuilder('c');
        $courses =
            $qb->orderBy('c.couCode', 'ASC')
                ->getQuery()
                ->getArrayResult();

        return $courses;

    }

    public function getSales($category)
    {

        $qb = $this->createQueryBuilder('c');

        $qb->add('select', 'c,cc')
            ->add('from', 'AppBundle:Course c')
            ->join('c.categories', 'cc')
            ->where('c.couSale IS NOT NULL');

        if (!is_null($category)) {

            $qb->andWhere('cc.catName LIKE :category')
                ->setParameter('category', $category);
        }

        $courses = $qb->orderBy('c.couCode', 'ASC')
            ->getQuery()
            ->getArrayResult();

        return $courses;

    }

}