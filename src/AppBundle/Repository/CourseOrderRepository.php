<?php
/**
 * Created by PhpStorm.
 * User: kenik
 * Date: 4.8.15
 * Time: 11:13
 */
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CourseOrderRepository extends EntityRepository
{

    public function getAllCourses()
    {

        $qb = $this->createQueryBuilder('o');
        $orders =
            $qb->add('select', 'o,u,c')
                ->add('from', 'AppBundle:CourseOrder o')
                ->join('o.user', ' u')
                ->join('o.course', 'c')
                ->orderBy('c.couCode', 'ASC')
                ->getQuery()
                ->getArrayResult();

        return $orders;

    }

}